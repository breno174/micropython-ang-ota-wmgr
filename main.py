import uwebsocket
import usocket as socket
from machine import Pin
import time
import network
import machine

mac_address = network.WLAN().config('mac')
unique_id = machine.unique_id()

dados = {
    "mac_address": ":".join("{:02x}".format(b) for b in mac_address),
    "unique_id": ":".join("{:02x}".format(b) for b in unique_id),
}
print(dados)

led_pin = 2
led = Pin(led_pin, Pin.OUT)

def piscar_led(tempo):
    led.value(1)
    time.sleep(tempo)
    led.value(0)
    time.sleep(tempo)

websocket_url = "ws://servidor-websocket.com"
evento_esperado = "evento"

def processar_evento(evento):
    print(f"Evento recebido: {evento}")

def consumir_websocket():
    try:
        ws = uwebsocket.websocket(socket.create_connection(websocket_url))

        while True:
            mensagem = ws.recv()

            if evento_esperado in mensagem:
                processar_evento(mensagem)

    except Exception as e:
        print("Erro:", e)

# Loop
while True:
    piscar_led(2)
    # consumir_websocket()
