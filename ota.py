
import network
import urequests
import os
import json
import machine
import time
import gc

class OTAUpdater:
    """ This class handles OTA updates. It connects to the Wi-Fi, checks for updates, downloads and installs them."""
    def __init__(self, ssid, password, repo_url, filename):
        self.filename = filename
        self.ssid = ssid
        self.password = password
        self.repo_url = repo_url
        self.latest_version = 0
        self.token = ''

        # self.version_url = repo_url + 'main/version.json'                 # Replacement of the version mechanism by Github's oid
        self.version_url = self.process_version_url(repo_url, filename)     # Process the new version url
        self.firmware_url = repo_url + filename                             # Removal of the 'main' branch to allow different sources

        # get the current version (stored in version.json)
        if 'version.json' in os.listdir():
            with open('version.json') as file:
                self.current_version = json.load(file)['version']
            print(f"Current device firmware version is '{self.current_version}'")

        else:
            self.current_version = "0"
            # save the current version
            with open('version.json', 'w') as f:
                json.dump({'version': self.current_version}, f)
  
    def process_version_url(self, repo_url, filename):
        """ Convert the file's url to its assoicatied version based on Github's oid management."""

        # Necessary URL manipulations
        # version_url = repo_url.replace("raw.githubusercontent.com", "github.com")  # Change the domain
        # version_url = version_url.replace("/", "搂", 4)                             # Temporary change for upcoming replace
        # version_url = version_url.replace("/", "/latest-commit/", 1)                # Replacing for latest commit
        # version_url = version_url.replace("搂", "/", 4)                             # Rollback Temporary change
        version_url = repo_url + filename                                       # Add the targeted filename
        
        return version_url

    def connect_wifi(self):
        """ Connect to Wi-Fi."""

        sta_if = network.WLAN(network.STA_IF)
        sta_if.active(True)
        sta_if.connect(self.ssid, self.password)
        while not sta_if.isconnected():
            print('.')
            time.sleep(0.5)
        print(f'Connected to WiFi, IP is: {sta_if.ifconfig()[0]}')
  
    def update_no_reset(self):
        """ Update the code without resetting the device."""

        # Save the fetched code and update the version file to latest version.
        with open('latest_code.py', 'w') as f:
            f.write(self.latest_code)
        
        # update the version in memory
        self.current_version = self.latest_version

        # save the current version
        with open('version.json', 'w') as f:
            json.dump({'version': self.current_version}, f)
        
        # free up some memory
        self.latest_code = None

        # Overwrite the old code.
        os.rename('latest_code.py', self.filename)
        
    def update_and_reset(self):
        """ Update the code and reset the device."""

        print('Updating device...')

        # Overwrite the old code.
        os.rename('latest_code.py', self.filename)
        
        print('last code')
        machine.reset()
        
    def fetch_latest_code(self)->bool:
        """ Fetch the latest code from the repo, returns False if not found."""
        gc.collect()
        # Fetch the latest code from the repo.
        # headers = {"accept": "application/json"}
        print(self.version_url)
        headers = {"accept": "application/json", "PRIVATE-TOKEN": self.token}
        response = urequests.get(self.version_url, headers=headers)
        if response.status_code == 200:
            print(f'Fetched latest firmware code, status: {response.status_code}, -  {response.text}')            
            self.latest_code = response.text
            response = None
            return True
        
        elif response.status_code == 404:
            print('Firmware not found.')
            return False
        elif response.status_code == 401:
            print('Invalid Token.')
            return False
        else:
            print("another error")
            return False
    
    def update_version_json(self):
        gc.collect()
        json_url = self.version_url.replace(self.filename, 'version.json')
        print(f'json_url: {json_url}')
        headers = {"accept": "application/json", "PRIVATE-TOKEN": self.token}
        response = urequests.get(json_url, headers=headers)
        
        # save the current version
        with open('version.json', 'w') as f:
            # json.dump({'version': self.current_version}, f)
            f.write(response.json())
        
        response = None
        return 1
        
    def check_for_updates(self):
        """ Check if updates are available."""
        
        if 'version.json' in os.listdir():
            with open('version.json') as file:
                self.current_version = json.load(file)['version']
            print(f"Current device firmware version is '{self.current_version}'")

        else:
            self.current_version = "0"
            # save the current version
            with open('version.json', 'w') as f:
                json.dump({'version': self.current_version}, f)

        print('Checking for latest version...')
        gc.collect()
        json_url = self.version_url.replace(self.filename, 'version.json')
        headers = {"accept": "application/json", "PRIVATE-TOKEN": self.token}
        response = urequests.get(json_url, headers=headers)
        self.latest_version = response.json()['version']      
        print(f'latest version is: {self.latest_version}')
        
        # compare versions
        newer_version_available = True if self.current_version != self.latest_version else False
        
        print(f'Newer version available: {newer_version_available}')
        return newer_version_available
  
    def download_and_install_update_if_available(self):
        """ Check for updates, download and install them."""
        # Connect to Wi-Fi
        # self.connect_wifi()
        # if self.check_for_updates():
        try:
            self.check_for_updates()
        except:
            print('deu errado checar a versao')

        if self.fetch_latest_code():
            self.update_no_reset()
            # self.update_and_reset()
            # update_version_json()   # alterar o arquivo version.json
        else:
            print('No new updates available.')
